# coding=utf-8
import typing as t
from clinic_personnel.classes.workers import Worker


class Department(object):
    """Отдел"""

    def __new__(cls, uid: int, name: str, chief: Worker = None):
        """Инициализация экземпляра класса отдел с проверкой данных"""
        if not isinstance(uid, int):  # не числовой uid недопустим
            raise ValueError('Не числовой идентификатор недопустим')
        if not name or not isinstance(name, str):  # name должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое название отдела недопустимо')
        if chief is not None and not isinstance(chief, Worker):  # chief должен быть типом Worker
            raise ValueError('Данные о начальнике должны храниться в экземпляре класса Worker')
        # инициализируем
        instance = super().__new__(cls)
        return instance

    def __init__(self, uid: int, name: str, chief: Worker = None):
        """Отдел"""
        self._uid = uid  # идентификатор
        self._name = name  # название
        self._chief = chief  # начальник
        self._workers = []
        self._sub_departments = []
        self._head_department = None

    def get_uid(self) -> int:
        """Получить ID отдела"""
        return self._uid

    def get_name(self) -> str:
        """Отдать название"""
        return self._name

    def set_name(self, name: str) -> None:
        """Изменить название отдела"""
        if not name or not isinstance(name, str):  # name должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое название отдела недопустимо')
        self._name = name

    def get_chief(self) -> Worker:
        """Получить начальника"""
        return self._chief

    def set_chief(self, chief: Worker) -> None:
        """Назначить начальника"""
        if not isinstance(chief, Worker):  # chief должен быть типом Worker
            raise ValueError('Данные о начальнике должны храниться в экземпляре класса Worker')
        self._chief = chief

    def rm_chief(self):
        """Убрать начальника"""
        self._chief = None

    def have_chief(self) -> bool:
        """Есть ли начальник?"""
        if self._chief:
            return True
        return False

    def get_workers(self) -> t.List[Worker]:
        """Получить список сотрудников"""
        return self._workers

    def get_worker_by_uid(self, uid: int) -> Worker:
        """Поиск сотрудника по ID"""
        for worker in self._workers:
            if worker.get_uid() == uid:
                return worker
        raise ValueError('Данные о сотруднике не найдены')

    def add_worker(self, worker: Worker) -> None:
        """Добавить сотрудника"""
        if not isinstance(worker, Worker):  # worker должен быть типом Worker
            raise ValueError('Данные о сотруднике должны храниться в экземпляре класса Worker')
        self._workers.append(worker)

    def rm_worker(self, worker: Worker) -> None:
        """Удаление сотрудника / Исключить сотрудника"""
        self._workers.remove(worker)  # бросает исключение ValueError если такого сотрудника нет

    def get_sub_departments(self) -> t.List:
        """Отдать список подотделов"""
        return self._sub_departments

    def add_sub_department(self, department) -> None:
        """
        Добавить подотдел
        (вызывать только из методов: set_head_department родительского отдела)
        """
        if not isinstance(department, Department):  # department должен быть типом Department
            raise ValueError('Данные об отделе должны храниться в экземпляре класса Department')
        self._sub_departments.append(department)

    def rm_sub_department(self, department) -> None:
        """
        Удалить подотдел
        (вызывать только из методов: set_head_department и rm_head_department родительского отдела)
        """
        self._sub_departments.remove(department)  # бросает исключение ValueError если такого подотдела нет

    def get_head_department(self):
        """Получить родительский отдел"""
        return self._head_department

    def set_head_department(self, department) -> None:
        """Назначить родительский отдел"""
        if not isinstance(department, Department):  # department должен быть типом Department
            raise ValueError('Данные о родительском отделе должны храниться в экземпляре класса Department')
        if self._head_department:  # удаляем себя из списка подотдела у текущего родителя
            self._head_department.rm_sub_department(self)
        self._head_department = department
        self._head_department.add_sub_department(self)  # добавляем себя в список подотделов у нового родителя

    def rm_head_department(self) -> None:
        """Удалить родительский отдел"""
        self._head_department.rm_sub_department(self)  # удаляем себя из списка подотдела у родителя
        self._head_department = None

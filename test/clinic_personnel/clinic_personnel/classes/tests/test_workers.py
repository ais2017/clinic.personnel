# coding=utf-8
import pytest

from clinic_personnel.classes.workers import Worker

"""
Чек-лист:
    Позитивные:
        1. Проверка успешной инициализации класса сотрудник и получения данных из геттеров
        2. Проверка успешного обновления данных о сотруднике
        3. Проверка успешного увольнения сотрудника
        4. Проверка успешного обратного найма сотрудника
    Негативные:
        1. Проверка ошибки инициализации класса сотрудник при некоректных данных
        2. Проверка неудачного увольнения сотрудника (был уволен ранее)
        3. Проверка неудачного обратного найма сотрудника (и так является действующим)
"""


# Позитивные
@pytest.mark.workers_positive
def test_valid_worker_init():
    """Проверка успешной инициализации класса сотрудник и получения данных из геттеров"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')
    assert worker.get_uid() == 1
    assert worker.fio() == '2'
    assert worker.birthday() == '3'
    assert worker.city() == '4'
    assert worker.passport() == '5'
    assert worker.phone() == '6'
    assert worker.email() == '7'
    assert worker.get_first_work_day() is not None
    assert worker.get_last_work_day() is None
    assert worker.position() == '8'
    assert worker.is_active()


@pytest.mark.workers_positive
def test_valid_worker_update_info():
    """Проверка успешного обновления данных о сотруднике"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')
    assert worker.get_uid() == 1
    assert worker.fio() == '2'
    assert worker.birthday() == '3'
    assert worker.city() == '4'
    assert worker.passport() == '5'
    assert worker.phone() == '6'
    assert worker.email() == '7'
    assert worker.get_first_work_day() is not None
    assert worker.get_last_work_day() is None
    assert worker.position() == '8'
    assert worker.is_active()

    worker.fio('new2')
    assert worker.fio() == 'new2'

    worker.birthday('new3')
    assert worker.birthday() == 'new3'

    worker.city('new4')
    assert worker.city() == 'new4'

    worker.passport('new5')
    assert worker.passport() == 'new5'

    worker.phone('new6')
    assert worker.phone() == 'new6'

    worker.email('new7')
    assert worker.email() == 'new7'

    worker.position('new8')
    assert worker.position() == 'new8'


@pytest.mark.workers_positive
def test_valid_worker_leave():
    """Проверка успешного увольнения сотрудника"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')
    assert worker.get_first_work_day() is not None
    assert worker.get_last_work_day() is None
    assert worker.is_active()

    worker.set_last_work_day()
    assert worker.get_last_work_day() is not None
    assert not worker.is_active()


@pytest.mark.workers_positive
def test_valid_worker_get_back():
    """Проверка успешного обратного найма сотрудника"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')
    assert worker.get_first_work_day() is not None
    assert worker.get_last_work_day() is None
    assert worker.is_active()

    worker.set_last_work_day()
    assert worker.get_last_work_day() is not None
    assert not worker.is_active()

    prev_date = worker.get_first_work_day()

    worker.set_new_first_work_day()
    assert worker.get_first_work_day() is not None
    assert worker.get_last_work_day() is not None
    assert worker.is_active()
    assert prev_date <= worker.get_first_work_day()


# Негативные
@pytest.mark.workers_negative
def test_invalid_worker_init():
    """Проверка ошибки инициализации класса сотрудник при некоректных данных"""
    with pytest.raises(ValueError, match="Не числовой идентификатор недопустим"):
        Worker(uid='', fio='', birthday='', city='', passport='', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое fio недопустимо"):
        Worker(uid=1, fio='', birthday='', city='', passport='', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое birthday недопустимо"):
        Worker(uid=1, fio='2', birthday='', city='', passport='', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое city недопустимо"):
        Worker(uid=1, fio='2', birthday='3', city='', passport='', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое passport недопустимо"):
        Worker(uid=1, fio='2', birthday='3', city='4', passport='', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое phone недопустимо"):
        Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое email недопустимо"):
        Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='', position='')
    with pytest.raises(ValueError, match="Пустое и не стрококвое position недопустимо"):
        Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='')


@pytest.mark.workers_negative
def test_invalid_worker_leave():
    """Проверка ошибки инициализации класса сотрудник при некоректных данных"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')

    worker.set_last_work_day()
    assert worker.get_last_work_day() is not None
    assert not worker.is_active()

    with pytest.raises(ValueError, match="Сотрудник уже уволен"):
        worker.set_last_work_day()


@pytest.mark.workers_negative
def test_invalid_worker_get_back():
    """Проверка ошибки инициализации класса сотрудник при некоректных данных"""
    worker = Worker(uid=1, fio='2', birthday='3', city='4', passport='5', phone='6', email='7', position='8')
    with pytest.raises(ValueError, match="Сотрудник действующий"):
        worker.set_new_first_work_day()

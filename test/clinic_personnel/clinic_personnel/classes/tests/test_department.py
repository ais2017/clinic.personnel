# coding=utf-8
import pytest

from clinic_personnel.classes.department import Department
from clinic_personnel.classes.workers import Worker

"""
Чек-лист:
    Позитивные:
        1. Проверка успешной инициализации без начальника и получения данных из геттеров
        2. Проверка успешной инициализации с начальником и получения данных из геттеров
        3. Успешное изменение названия отдела
        4. Успешное изменение начальника отдела
        5. Успешное удаление начальника отдела и проверка его наличия (до и после)
        6. Успешное добавление сотрудника и проверка получения сотрудника по его ID
        7. Успешное удаление сотрудника
        8. Успешное добавление родительского отдела вместо None (у родительского отдела в подотделы добавился наш отдел)
        9. Успешное добавление родительского отдела вместо старого (у старого удалилось, новому добавилось)
        10. Успешное удаление родительского отдела (у родительского отдела из подотделов удалился наш отдел)
        11. Успешное добавление подотдела
        12. Успешное удаление подотдела
    Негативные:
        1. Проверка ошибки инициализации класса отдела при некоректных данных (идентификатор)
        2. Проверка ошибки инициализации класса отдела при некоректных данных (название пустое)
        3. Проверка ошибки инициализации класса отдела при некоректных данных (название не строка)
        4. Проверка ошибки инициализации класса отдела при некоректных данных (начальник не типа Worker)
        5. Неудачное изменение названия отдела
        6. Неудачное изменение начальника отдела (неверный тип)
        7. Неудачное добавление сотрудника (неверный тип)
        8. Неудачное получения сотрудника по его ID (сотрудника нет в списке)
        9. Неудачное удаление сотрудника (такого сотрудника не существует)
        10. Неудачное добавление родительского отдела (неверный тип)
        11. Неудачное добавление подотдела (неверный тип)
        12. Неудачное удаление подотдела (нет такого подотдела)
"""


# Позитивные
@pytest.mark.department_positive
def test_valid_init_department_without_chief():
    """Проверка успешной инициализации без начальника и получения данных из геттеров"""
    department = Department(uid=1, name='my', chief=None)
    assert department.get_uid() == 1
    assert department.get_name() == 'my'
    assert department.get_chief() is None
    assert len(department.get_workers()) == 0
    assert len(department.get_sub_departments()) == 0
    assert department.get_head_department() is None


@pytest.mark.department_positive
def test_valid_init_department_with_chief():
    """Проверка успешной инициализации с начальником и получения данных из геттеров"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert department.get_uid() == 1
    assert department.get_name() == 'my'
    assert department.get_chief() == worker


@pytest.mark.department_positive
def test_success_update_department_name():
    """Успешное изменение названия отдела"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert department.get_name() == 'my'

    department.set_name('my new name')
    assert department.get_name() == 'my new name'


@pytest.mark.department_positive
def test_success_change_department_chief():
    """Успешное изменение начальника отдела"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    worker_2 = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert worker == department.get_chief()

    department.set_chief(worker_2)
    assert worker_2 == department.get_chief()


@pytest.mark.department_positive
def test_success_check_and_set_department_chief():
    """Успешное удаление начальника отдела и проверка его наличия (до и после)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my')
    assert not department.have_chief()

    department.set_chief(worker)
    assert department.have_chief()


@pytest.mark.department_positive
def test_success_add_worker_to_department_and_get_it():
    """Успешное добавление сотрудника и проверка получения сотрудника по его ID"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert len(department.get_workers()) == 0

    department.add_worker(worker)
    assert len(department.get_workers()) == 1
    assert worker in department.get_workers()

    assert worker == department.get_worker_by_uid(worker.get_uid())


@pytest.mark.department_positive
def test_success_rm_worker_from_department():
    """Успешное удаление сотрудника"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert len(department.get_workers()) == 0

    department.add_worker(worker)
    assert len(department.get_workers()) == 1
    assert worker in department.get_workers()

    department.rm_worker(worker)
    assert len(department.get_workers()) == 0
    assert worker not in department.get_workers()


@pytest.mark.department_positive
def test_success_set_head_department():
    """Успешное добавление родительского отдела вместо None (у родительского отдела в подотделы добавился наш отдел)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='parent', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    assert len(department.get_sub_departments()) == 0

    department_child.set_head_department(department)
    assert len(department.get_sub_departments()) == 1
    assert department_child in department.get_sub_departments()


@pytest.mark.department_positive
def test_success_update_head_department():
    """Успешное добавление родительского отдела вместо старого (у старого удалилось, новому добавилось)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department_par_old = Department(uid=1, name='parent old', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    department_par_new = Department(uid=3, name='parent new', chief=worker)
    assert len(department_par_new.get_sub_departments()) == 0
    assert len(department_par_old.get_sub_departments()) == 0

    department_child.set_head_department(department_par_old)
    assert len(department_par_new.get_sub_departments()) == 0
    assert department_child not in department_par_new.get_sub_departments()
    assert len(department_par_old.get_sub_departments()) == 1
    assert department_child in department_par_old.get_sub_departments()

    department_child.set_head_department(department_par_new)
    assert len(department_par_new.get_sub_departments()) == 1
    assert department_child in department_par_new.get_sub_departments()
    assert len(department_par_old.get_sub_departments()) == 0
    assert department_child not in department_par_old.get_sub_departments()


@pytest.mark.department_positive
def test_success_rm_head_department():
    """Успешное удаление родительского отдела (у родительского отдела из подотделов удалился наш отдел)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='parent', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    assert len(department.get_sub_departments()) == 0

    department_child.set_head_department(department)
    assert len(department.get_sub_departments()) == 1
    assert department_child in department.get_sub_departments()

    department_child.rm_head_department()
    assert len(department.get_sub_departments()) == 0
    assert department_child not in department.get_sub_departments()


@pytest.mark.department_positive
def test_success_add_sub_department():
    """Успешное добавление подотдела"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='parent', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    assert len(department.get_sub_departments()) == 0

    department.add_sub_department(department_child)
    assert len(department.get_sub_departments()) == 1
    assert department_child in department.get_sub_departments()


@pytest.mark.department_positive
def test_success_rm_sub_department():
    """Успешное удаление подотдела"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='parent', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    assert len(department.get_sub_departments()) == 0

    department.add_sub_department(department_child)
    assert len(department.get_sub_departments()) == 1
    assert department_child in department.get_sub_departments()

    department.rm_sub_department(department_child)
    assert len(department.get_sub_departments()) == 0
    assert department_child not in department.get_sub_departments()


# Негативные
@pytest.mark.department_negative
def test_invalid_department_init_uid():
    """Проверка ошибки инициализации класа отдела при некоректных данных (идентификатор)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    with pytest.raises(ValueError, match="Не числовой идентификатор недопустим"):
        Department(uid='', name='my', chief=worker)


@pytest.mark.department_negative
def test_invalid_department_init_empty_name():
    """Проверка ошибки инициализации класа отдела при некоректных данных (название пустое)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    with pytest.raises(ValueError, match="Пустое и не стрококвое название отдела недопустимо"):
        Department(uid=1, name='', chief=worker)


@pytest.mark.department_negative
def test_invalid_department_init_name_not_str():
    """Проверка ошибки инициализации класа отдела при некоректных данных (название не строка)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    with pytest.raises(ValueError, match="Пустое и не стрококвое название отдела недопустимо"):
        Department(uid=1, name=['123'], chief=worker)


@pytest.mark.department_negative
def test_invalid_department_init_chief_not_worker():
    """Проверка ошибки инициализации класа отдела при некоректных данных (начальник не типа Worker)"""
    with pytest.raises(ValueError, match="Данные о начальнике должны храниться в экземпляре класса Worker"):
        Department(uid=1, name='my', chief='i am worker')


@pytest.mark.department_negative
def test_fail_department_update_name():
    """Неудачное изменение названия отдела"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert department.get_name() == 'my'
    with pytest.raises(ValueError, match="Пустое и не стрококвое название отдела недопустимо"):
        department.set_name('')
    with pytest.raises(ValueError, match="Пустое и не стрококвое название отдела недопустимо"):
        department.set_name(['my'])
    assert department.get_name() == 'my'


@pytest.mark.department_negative
def test_fail_department_update_chief():
    """Неудачное изменение начальника отдела (неверный тип)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert department.get_chief() == worker
    with pytest.raises(ValueError, match='Данные о начальнике должны храниться в экземпляре класса Worker'):
        department.set_chief('i am worker')
    assert department.get_chief() == worker


@pytest.mark.department_negative
def test_fail_department_add_worker():
    """Неудачное добавление сотрудника (неверный тип)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert len(department.get_workers()) == 0
    with pytest.raises(ValueError, match='Данные о сотруднике должны храниться в экземпляре класса Worker'):
        department.add_worker('i am worker')
    assert len(department.get_workers()) == 0


@pytest.mark.department_negative
def test_fail_department_get_worker_by_uid():
    """Неудачное получения сотрудника по его ID (сотрудника нет в списке)"""
    department = Department(uid=1, name='my')
    assert len(department.get_workers()) == 0
    with pytest.raises(ValueError, match='Данные о сотруднике не найдены'):
        department.get_worker_by_uid(1)


@pytest.mark.department_negative
def test_fail_department_rm_worker():
    """Неудачное удаление сотрудника (такого сотрудника не существует)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='my', chief=worker)
    assert len(department.get_workers()) == 0
    department.add_worker(worker)
    assert len(department.get_workers()) == 1
    with pytest.raises(ValueError):  # list.remove(x): x not in list
        department.rm_worker('i am worker')
    assert len(department.get_workers()) == 1


@pytest.mark.department_negative
def test_fail_department_add_head():
    """Неудачное добавление родительского отдела (неверный тип)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='child', chief=worker)
    assert department.get_head_department() is None
    with pytest.raises(ValueError,
                       match='Данные о родительском отделе должны храниться в экземпляре класса Department'):
        department.set_head_department('i am head department')
    assert department.get_head_department() is None


@pytest.mark.department_negative
def test_fail_add_sub_department():
    """Неудачное добавление подотдела (неверный тип)"""
    worker = Worker(uid=0, fio='1', birthday='1', city='1', passport='1', phone='1', email='1', position='1')
    department = Department(uid=1, name='parent', chief=worker)
    department_child = Department(uid=2, name='child', chief=worker)
    assert len(department.get_sub_departments()) == 0
    department.add_sub_department(department_child)
    assert len(department.get_sub_departments()) == 1
    with pytest.raises(ValueError, match='Данные об отделе должны храниться в экземпляре класса Department'):
        department.add_sub_department('i am sub department')
    assert len(department.get_sub_departments()) == 1


@pytest.mark.department_negative
def test_fail_rm_sub_department():
    """Неудачное удаление подотдела (нет такого подотдела)"""
    department = Department(uid=1, name='parent')
    assert len(department.get_sub_departments()) == 0
    with pytest.raises(ValueError):  # list.remove(x): x not in list
        department.rm_sub_department('i am sub department')
    assert len(department.get_sub_departments()) == 0

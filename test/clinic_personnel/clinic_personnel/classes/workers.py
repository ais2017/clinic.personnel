# coding=utf-8
from datetime import datetime


class Worker(object):
    """Сотрудник"""

    def __new__(cls, uid: int, fio: str, birthday: str, city: str,
                passport: str, phone: str, email: str, position: str):
        """Инициализация экземпляра класса сотрудник с проверкой данных"""
        if not isinstance(uid, int):  # не числовой uid недопустим
            raise ValueError('Не числовой идентификатор недопустим')
        if not fio or not isinstance(fio, str):  # fio должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое fio недопустимо')
        if not birthday or not isinstance(birthday, str):  # birthday должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое birthday недопустимо')
        if not city or not isinstance(city, str):  # city должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое city недопустимо')
        if not passport or not isinstance(passport, str):  # passport должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое passport недопустимо')
        if not phone or not isinstance(phone, str):  # phone должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое phone недопустимо')
        if not email or not isinstance(email, str):  # email должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое email недопустимо')
        if not position or not isinstance(position, str):  # position должен быть типом str и не пустым
            raise ValueError('Пустое и не стрококвое position недопустимо')
        # инициализируем
        instance = super().__new__(cls)
        return instance

    def __init__(self, uid: int, fio: str, birthday: str, city: str,
                passport: str, phone: str, email: str, position: str):
        """Сотрудник"""
        self._uid = uid
        self._fio = fio
        self._birthday = birthday
        self._city = city
        self._passport = passport
        self._phone = phone
        self._email = email
        self._first_work_day = datetime.now()
        self._last_work_day = None
        self._position = position
        self._state = True

    def get_uid(self) -> int:
        """Получить ID"""
        return self._uid

    def fio(self, fio: str = None) -> str:
        """Получить\Изменить ФИО"""
        if fio:
            self._fio = fio
        return self._fio

    def birthday(self, birthday: str = None) -> str:
        """Получить\Изменить дату рождения"""
        if birthday:
            self._birthday = birthday
        return self._birthday

    def city(self, city: str = None) -> str:
        """Получить\Изменить город"""
        if city:
            self._city = city
        return self._city

    def passport(self, passport: str = None) -> str:
        """Получить\Изменить паспортные данные"""
        if passport:
            self._passport = passport
        return self._passport

    def phone(self, phone: str = None) -> str:
        """Получить\Изменить телефон"""
        if phone:
            self._phone = phone
        return self._phone

    def email(self, email: str = None) -> str:
        """Получить\Изменить почту"""
        if email:
            self._email = email
        return self._email

    def get_first_work_day(self) -> datetime:
        """Получить дату приёма на работу"""
        return self._first_work_day

    def get_last_work_day(self) -> datetime:
        """Получить дату увольнения"""
        return self._last_work_day

    def position(self, position: str = None) -> str:
        """Получить\Изменить должность"""
        if position:
            self._position = position
        return self._position

    def is_active(self) -> bool:
        """Является ли действующим сотрудником? (True -> да, False -> нет"""
        return self._state

    def set_last_work_day(self):
        """Уволить сотрудника"""
        if not self._state:
            raise ValueError('Сотрудник уже уволен')
        self._last_work_day = datetime.now()
        self._state = False

    def set_new_first_work_day(self):
        """Нанять сотрудника обратно"""
        if self._state:
            raise ValueError('Сотрудник действующий')
        self._first_work_day = datetime.now()
        self._state = True

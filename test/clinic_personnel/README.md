Установка нужных пакетов
========================
    sudo apt install python3 python3-pip python-django python-django-common
    sudo pip3 install pipenv

    pipenv install --python=python3.6 --dev


Начало работы
=============
1. Запуск проекта:

        pipenv run python manage.py makemigrations
        pipenv run python manage.py migrate
        pipenv run python manage.py runserver

2. Сервис будет доступен локально по адресу:

        http://127.0.0.1:8000/


Запуск тестов
-------------

    pipenv run py.test
